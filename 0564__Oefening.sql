CREATE DATABASE IF NOT EXISTS ModernWays;
USE ModernWays;

CREATE TABLE Opleidingen (
Naam VARCHAR(100) PRIMARY KEY
);

CREATE TABLE Lectoren (
Voornaam VARCHAR(100) NOT NULL,
Familienaam  VARCHAR(100) NOT NULL,
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY
);

CREATE TABLE Vakken (
Naam VARCHAR(100) PRIMARY KEY
);

CREATE TABLE Studenten (
Voornaam VARCHAR(100) NOT NULL,
Familienaam  VARCHAR(100) NOT NULL,
Studentennummer INT AUTO_INCREMENT PRIMARY KEY,
Semester TINYINT UNSIGNED NOT NULL,
Opleidingen_Naam VARCHAR(100) NOT NULL,
CONSTRAINT fk_Studenten_Opleidingen
FOREIGN KEY (Opleidingen_Naam)
REFERENCES Opleidingen(Naam)
);

CREATE TABLE Geeft (
Lectoren_Personeelsnummer INT NOT NULL,
Vakken_Naam VARCHAR(100) NOT NULL,
CONSTRAINT fk_Geeft_Lectoren
FOREIGN KEY (Lectoren_Personeelsnummer)
REFERENCES Lectoren(Personeelsnummer),
CONSTRAINT fk_Geeft_Vakken
FOREIGN KEY (Vakken_Naam)
REFERENCES Vakken(Naam)
);

CREATE TABLE Opleidingsonderdeel (
Vakken_Naam VARCHAR(100) NOT NULL,
Opleidingen_Naam VARCHAR(100) NOT NULL,
CONSTRAINT fk_Opleidingsonderdeel_Vakken
FOREIGN KEY (Vakken_Naam)
REFERENCES Vakken(Naam),
CONSTRAINT fk_Opleidingsonderdeel_Opleidingen
FOREIGN KEY (Opleidingen_Naam)
REFERENCES Opleidingen(Naam)
);